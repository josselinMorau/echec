"""
Pour jouer au jeu avec une interface cliquable.
"""

import pygame
from time import sleep
from pygame.locals import MOUSEMOTION, QUIT, MOUSEBUTTONUP
from copy import deepcopy
from . import pieces as pc
from .jeu import initPlateau, creerDictPortee, enEchec
from .ordi import choisirCoup
import sys

def menu(fenetre,posCurseur,choixCouleur):
	"""
	Affiche le menu principal.
	"""
	fichier = "./src/sprites/menu_principal%s.png" % ('_couleur' if choixCouleur else '')
	fondMenu = pygame.image.load(fichier).convert()
	fenetre.blit(fondMenu,(0,0))
	i = 0
	liste = ['blanc','noir','retour'] if choixCouleur else ['vsjoueur','vsordi','quitter']
	for mot in liste:
		fichier = './src/sprites/bouton_menu_%s%s.png' % (mot,'_highlight' if posCurseur == (0,i) else '')
		image = pygame.image.load(fichier).convert()
		image.set_colorkey((181,230,29))
		fenetre.blit(image,(371,202+i*125))
		i+=1
	pygame.display.update()

def actualiser(fenetre,fond,echiquier,posCurseur,boutonsPresses,piecesJouables=[],posPiece=(),portee=[]):
	"""
	Actualise l'affichage de l'echiquier.
	"""
	fenetre.blit(fond,(0,0))
	couleur = "blanche"
	for i in range(len(echiquier)):
		for j in range(len(echiquier[i])):
			couleur = "blanche" if couleur == "bleue" else "bleue"
			if (j,i) == posPiece:
				couleurCase = "rouge"
			elif (j,i) in portee:
				couleurCase = "verte"
			elif (j,i) in piecesJouables and boutonsPresses['jouables']:
				couleurCase = "violette"
			else:
				couleurCase = couleur
			fichier = "./src/sprites/case_%s%s.png" % (couleurCase,"_highlight" if (j,i) == posCurseur else "")
			case = pygame.image.load(fichier).convert()
			fenetre.blit(case,(38+j*80,2+i*80))
			if echiquier[i][j] is not None:
				pion = echiquier[i][j]
				fichier = "./src/sprites/%s_%s.png" % (pion.nom,pion.couleur)
				case = pygame.image.load(fichier).convert()
				case.set_colorkey((181,230,29))
				fenetre.blit(case,(38+j*80,2+i*80))
		couleur = "blanche" if couleur == "bleue" else "bleue"
	for mot in ['jouables','abandonner']:
		posBouton = (713,60) if mot == 'jouables' else (713,540)
		fichier = "./src/sprites/bouton_%s_%s.png" % (mot,'appuye' if boutonsPresses[mot] else 'relache')
		bouton = pygame.image.load(fichier).convert()
		bouton.set_colorkey((181,230,29))
		fenetre.blit(bouton,posBouton)
	pygame.display.update()

def afficherMessage(fenetre,fond,message,arret=False):
	"""
	Affiche un message a l'ecran.
	"""
	compteur = 9
	while compteur:
		fichier = "./src/sprites/carton_%i.png" % compteur
		image = pygame.image.load(fichier).convert()
		image.set_colorkey((181,230,29))
		fenetre.blit(image,(0,242))
		pygame.display.update()
		sleep(0.01)
		compteur -=1
	fichier = "./src/sprites/message_%s.png" % message
	image = pygame.image.load(fichier).convert()
	image.set_colorkey((181,230,29))
	fenetre.blit(image,(0,242))
	pygame.display.update()
	sleep(0.8)
	if arret:
		attendre = True
		while attendre:
			for event in pygame.event.get():
				if event.type == QUIT:
					sys.exit(0)
				if event.type == MOUSEBUTTONUP:
					attendre = False

def promotion(fenetre,fond):
	"""
	Active une promotion.
	"""
	compteur = 9
	while compteur:
		fichier = "./src/sprites/carton_%i.png" % compteur
		image = pygame.image.load(fichier).convert()
		image.set_colorkey((181,230,29))
		fenetre.blit(image,(0,242))
		pygame.display.update()
		sleep(0.01)
		compteur -=1
	fichier = "./src/sprites/promotion.png"
	image = pygame.image.load(fichier).convert()
	image.set_colorkey((181,230,29))
	fenetre.blit(image,(0,242))
	pygame.display.update()
	sleep(0.8)
	attendre = True
	while attendre:
		for event in pygame.event.get():
			if event.type == QUIT:
				sys.exit(0)
			if event.type == MOUSEBUTTONUP:
				attendre = False

def positionCurseur(pos,menu=False):
	"""
	Retourne la position du curseur.
	"""
	(posX,posY) = (pos[0]-38,pos[1]-2)
	if menu and posX >= 333 and posX <= 559 and posY >= 200 and posY <= 300:
		return (0,0)
	if menu and posX >= 333 and posX <= 559 and posY >= 325 and posY <= 425:
		return (0,1)
	if menu and posX >= 333 and posX <= 559 and posY >= 450 and posY <= 550:
		return (0,2)
	if posX >= 675 and posX <= 900 and posY >= 58 and posY <= 157:
		return (713,60)
	if posX >= 675 and posX <= 900 and posY >= 538 and posY <= 638:
		return (713,540)
	if posX < 0 or posX > 640 or posX < 0 or posX > 640:
		return ()
	posX //= 80
	posY //= 80
	return (posX,posY)

def jouer(fenetre,ordi='aucun'):
	"""
	Lance une partie d'echecs.
	"""
	fond = pygame.image.load("./src/sprites/echiquier.png").convert()
	plateau = deepcopy(pc.Piece.PLATEAU)
	pions = {'blanc':[],'noir':[],'roi-blanc':None,'roi-noir':None}
	initPlateau(plateau,pions)
	posCurseur = positionCurseur(pygame.mouse.get_pos())
	boutonsPresses = {'abandonner': False, 'jouables': False}
	actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses)	
	pygame.key.set_repeat(400, 30)
	# phases: 0 : 'selectPiece' , 1 : 'selectDestination' , 2 :'finTour'
	posCurseur = ()
	joueurEnCours = "noir"
	mat = False
	while True:
		for event in pygame.event.get():
			pass
		posCurseur = positionCurseur(pygame.mouse.get_pos())
		joueurEnCours = 'blanc' if joueurEnCours == 'noir' else 'noir'
		actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses)
		if boutonsPresses['abandonner']:
			sleep(0.2)
			afficherMessage(fenetre,fond,"abandon")
			break
		dictPortee = creerDictPortee(plateau,joueurEnCours)
		if not dictPortee:
			if enEchec(plateau,joueurEnCours):
				mat = True
				sleep(0.2)
				afficherMessage(fenetre,fond,"echecetmat")
			else:
				sleep(0.2)
				afficherMessage(fenetre,fond,"pat",arret=True)
			break
		phaseDeJeu = 0
		posPiece = ()
		porteeCourante = []
		sleep(0.2)
		afficherMessage(fenetre,fond,"tourdes%ss" % joueurEnCours)
		actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses,piecesJouables=list(dictPortee.keys()),posPiece=posPiece)
		if joueurEnCours == ordi:
			actualiser(fenetre,fond,plateau,(),boutonsPresses,piecesJouables=list(dictPortee.keys()))
			sleep(0.7)
			dep,dest = choisirCoup(plateau,dictPortee,joueurEnCours)
			actualiser(fenetre,fond,plateau,dep,boutonsPresses,piecesJouables=list(dictPortee.keys()))
			sleep(0.7)
			actualiser(fenetre,fond,plateau,dest,boutonsPresses,posPiece=dep,portee=dictPortee[dep])
			sleep(0.7)
			plateau[dep[1]][dep[0]].deplacer(dest)
		else:
			while phaseDeJeu != 2:
				for event in pygame.event.get():
					if event.type == QUIT:
						sys.exit(0)
					if event.type == MOUSEMOTION:
						posCurseur = positionCurseur(pygame.mouse.get_pos())
						piecesJouables = list(dictPortee.keys()) if phaseDeJeu == 0 else []
						actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses,piecesJouables=piecesJouables,posPiece=posPiece,portee=porteeCourante)
					if event.type == MOUSEBUTTONUP:
						if phaseDeJeu == 0 and posCurseur in dictPortee.keys():
							posPiece = posCurseur
							porteeCourante = dictPortee[posPiece]
							phaseDeJeu = 1
							actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses,posPiece=posPiece,portee=porteeCourante)
						elif phaseDeJeu == 1 and posCurseur in porteeCourante:
							phaseDeJeu = 2
							plateau[posPiece[1]][posPiece[0]].deplacer(posCurseur)
						else:
							if posCurseur == (713,60):
								boutonsPresses['jouables'] = not boutonsPresses['jouables']
								phaseDeJeu = 0
							elif posCurseur == (713,540):
								boutonsPresses['abandonner'] = not boutonsPresses['abandonner']
								phaseDeJeu = 2
							else:
								phaseDeJeu = 0
							posPiece = ()
							porteeCourante = []
							piecesJouables = list(dictPortee.keys())
							actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses,piecesJouables,posPiece=posPiece,portee=porteeCourante)
	if mat or boutonsPresses['abandonner']:
		actualiser(fenetre,fond,plateau,posCurseur,boutonsPresses)
		sleep(0.2)
		afficherMessage(fenetre,fenetre,"victoiredes%ss" % joueurEnCours,arret=True)


def demarrer():
	"""
	Demarre le programme en mode interface.
	"""
	pygame.init()
	fenetre = pygame.display.set_mode((960, 680))
	posCurseur = ()
	choixCouleur = False
	menu(fenetre,posCurseur,choixCouleur)
	while True:
		for event in pygame.event.get():
			if event.type == QUIT:
				sys.exit(0)
			if event.type == MOUSEMOTION:
				posCurseur = positionCurseur(pygame.mouse.get_pos(),True)
				menu(fenetre,posCurseur,choixCouleur)
			if event.type == MOUSEBUTTONUP:
				if choixCouleur:
					if posCurseur == (0,0):
						jouer(fenetre,'noir')
						choixCouleur = False
						menu(fenetre,posCurseur,choixCouleur)
					elif posCurseur == (0,1):
						jouer(fenetre,'blanc')
						choixCouleur = False
						menu(fenetre,posCurseur,choixCouleur)
					elif posCurseur == (0,2):
						choixCouleur = False
						menu(fenetre,posCurseur,choixCouleur)
				else:
					if posCurseur == (0,0):
						jouer(fenetre)
					elif posCurseur == (0,1):
						choixCouleur = True
						menu(fenetre,posCurseur,choixCouleur)
					elif posCurseur == (0,2):
						sys.exit(0)
