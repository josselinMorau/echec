from . import pieces as pc

def enEchec(plateau,couleur):
    """
    Retourne True si le roi de la couleur donnee est en echec.
    """
    porteeTotale = []
    posRoi = None
    for ligne in plateau:
        for case in ligne:
            if case is not None:
                if case.couleur != couleur:
                    porteeTotale += case.portee()
                elif case.nom == 'roi':
                    posRoi = case.pos
    return posRoi in porteeTotale

def peutRoque(roi,tour):
    """
    Retourne True si un roque est possible entre le roi et la tour donnes.
    """
    if tour is None or roi is None:
        return False
    (roiX,y) = roi.pos
    tourX = tour.posX
    if roi.estDeplace or tour.estDeplace:
        return False
    portee = []
    for ligne in roi.plateau:
        for case in ligne:
            if case is not None and case.couleur != roi.couleur:
                portee += case.portee()
    (dep,fin) = (roiX,roiX+3) if roiX < tourX else (roiX-3,roiX+1)
    for xi in range(dep,fin):
        caseEvaluee = roi.plateau[y][xi]
        if (caseEvaluee is not None and caseEvaluee is not roi) or (xi,y) in portee:
            return False
    return True

def creerDictPortee(plateau,couleur):
    """
    Retourne un dictionnaire avec pour clefs les positions des pieces jouables et pour valeurs leur portee.
    """
    dictPortee = {}
    roi = None
    tour1=None
    tour2=None
    for ligne in plateau:
        for case in ligne:
            if case is not None and case.couleur == couleur:
                if case.nom == 'roi':
                    roi = case
                elif case.nom == 'tour':
                    if tour1 is None:
                        tour1 = case
                    else:
                        tour2 = case
                pionCourant = case
                portee = []
                posIni = pionCourant.pos
                for pos in pionCourant.portee():
                    piece = plateau[pos[1]][pos[0]]
                    pionCourant.deplacer(pos,simu=True)
                    if not enEchec(plateau,couleur):
                        portee.append(pos)
                    pionCourant.deplacer(posIni,simu=True)
                    plateau[pos[1]][pos[0]] = piece
                if portee :
                    dictPortee[posIni] = portee
    for tour in [tour1,tour2]:
        if peutRoque(roi,tour):
            dictPortee[roi.pos].append(tour.pos)
    return dictPortee

def verifPromotion(plateau,couleur):
    """
    Verifie si un pion de la couleur donnee est promu et le retourne.
    Retourne None sinon.
    """
    y = 0 if couleur == 'blanc' else 7
    for i in range(8):
        if plateau[y][i] is not None and plateau[y][i].nom == 'pion':
            return plateau[y][i]
    return None

def initPlateau(plateau,pions):
    """
    Initialise le plateau d'echecs.
    """
    lignepions = {'blanc':6,'noir':1}
    ligneroi = {'blanc':7,'noir':0}
    #Pose des pions
    for i in range(8):
        for couleur in ['blanc','noir']:
            j = lignepions[couleur]
            plateau[j][i] = pc.Pion(couleur,i,j,plateau)
            pions[couleur].append(plateau[j][i])
    #Pose des tours
    for i in [0,7]:
        for couleur in ['blanc','noir']:
            j = ligneroi[couleur]
            plateau[j][i] = pc.Tour(couleur,i,j,plateau)
            pions[couleur].append(plateau[j][i])
    #Pose des cavaliers
    for i in [1,6]:
        for couleur in ['blanc','noir']:
            j = ligneroi[couleur]
            plateau[j][i] = pc.Cavalier(couleur,i,j,plateau)
            pions[couleur].append(plateau[j][i])
    #Pose des fous
    for i in [2,5]:
        for couleur in ['blanc','noir']:
            j = ligneroi[couleur]
            plateau[j][i] = pc.Fou(couleur,i,j,plateau)
            pions[couleur].append(plateau[j][i])
    #Pose des reines et des rois
    for couleur in ['blanc','noir']:
        j = ligneroi[couleur]
        plateau[j][4] = pc.Roi(couleur,4,j,plateau)
        pions['roi-'+couleur] = plateau[j][4]
        pions[couleur].append(plateau[j][4])
        plateau[j][3] = pc.Reine(couleur,3,j,plateau)
        pions[couleur].append(plateau[j][3])
