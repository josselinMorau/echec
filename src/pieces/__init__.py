from .piece import Piece
from .cavalier import Cavalier
from .fou import Fou
from .pion import Pion
from .reine import Reine
from .roi import Roi
from .tour import Tour