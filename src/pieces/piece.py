from copy import deepcopy

class Piece:
    """
    Classe abstraite representant une piece d'echec.
    """
    COULEURS = {'blanc':'\033[37m','noir':'\033[90m'}
    PLATEAU = [[None] * 8 for i in range(8)]
    NOM_PIECES = ['pion','tour','cavalier','fou','reine','roi']
    STR_PIECES = {'pion':'\u265f','roi':'\u265a','reine':'\u265b','tour':'\u265c','fou':'\u265d','cavalier':'\u265e'}

    def __init__(self,nom,couleur,posX,posY,plateau=deepcopy(PLATEAU)):
        self.__nom = nom
        self.__couleur = couleur
        self.__posX = posX
        self.__posY = posY
        self.__plateau = plateau
        self.__detruit = False
        self.__estDeplace = False
        plateau[posY][posX] = self
    
    def __str__(self):
        return Piece.COULEURS[self.couleur]+Piece.STR_PIECES['%s'% self.nom]+'\033[0m'
    
    def __repr__(self):
        return self.__str__()
    
    @property
    def nom(self):
        """
        Nom de la piece.
        """
        return self.__nom
    
    @nom.setter
    def nom(self,val):
        if val in Piece.NOM_PIECES:
            self.__nom = val
    
    @property
    def couleur(self):
        """
        Couleur de la piece ( 'noir' ou 'blanc' ).
        """
        return self.__couleur
    
    @property
    def plateau(self):
        """
        Plateau sur lequel est la piece.
        """
        return self.__plateau
    
    @property
    def estDetruit(self):
        """
        Booleen verifiant si la piece est detruite.
        """
        return self.__detruit
    
    @property
    def estDeplace(self):
        """
        True si la piece a ete deplacee au moins une fois.
        """
        return self.__estDeplace

    
    @property
    def posX(self):
        """
        Abscisse de la piece.
        """
        return self.__posX
    
    @posX.setter
    def posX(self,val):
        self.__posX = val
    
    @property
    def posY(self):
        """
        Ordonnee de la piece.
        """
        return self.__posY
    
    @posY.setter
    def posY(self,val):
        self.__posY = val
    
    @property
    def pos(self):
        """
        Position (x,y) de la piece.
        """
        return (self.__posX,self.__posY)
    
    def deplacer(self,pos,simu=False):
        """
        Deplace la piece a la position indiquee. Si une piece s'y trouve, la detruit.
        Retourne la piece detruite ou None si aucune piece n'est detruite.
        """
        (posX,posY) = pos
        if not simu and not self.estDeplace :
            self.__estDeplace = 'True'
        pieceDetruite = None
        if isinstance(self.plateau[posY][posX],Piece) :
            pieceDetruite = self.__plateau[posY][posX]
            self.__plateau[posY][posX].detruire()
        self.__plateau[posY][posX] = self
        self.__plateau[self.posY][self.posX] = None
        self.posX = posX
        self.posY = posY
        return pieceDetruite
    
    def detruire(self):
        """
        Detruit la piece.
        """
        self.__plateau[self.posY][self.posX] = None
        self.__detruit = True
    
    def portee(self,rec=True):
        """
        Retourne la liste des positions que la piece peut atteindre.
        """
        return []

    @staticmethod
    def positionValide(posX,posY):
        """
        Retourne True si la position donnee existe.
        """
        return posX in range(8) and posY in range(8)
    
    @staticmethod
    def positionEnnemie(posX,posY,couleur,plateau):
        """
        Retourne True si la position donnee est celle d'un ennemi.
        """
        return plateau[posY][posX] is not None and plateau[posY][posX].couleur != couleur
