from .piece import Piece
from copy import deepcopy

class Tour(Piece):
    """
    Classe representant une tour dans un jeu d'echec.
    """

    def __init__(self,couleur,posX,posY,plateau=deepcopy(Piece.PLATEAU)):
        super().__init__('tour',couleur,posX,posY,plateau)
    
    def portee(self,rec=True):
        portee = []
        x,y = self.posX,self.posY
        for xi in range(x-1,x-8,-1):
            if Piece.positionValide(xi,y):
                if self.plateau[y][xi] is not None:
                    if self.plateau[y][xi].couleur != self.couleur:
                        portee.append((xi,y))
                    break
                portee.append((xi,y))
        for xi in range(x+1,x+8):
            if Piece.positionValide(xi,y):
                if self.plateau[y][xi] is not None:
                    if self.plateau[y][xi].couleur != self.couleur:
                        portee.append((xi,y))
                    break
                portee.append((xi,y))
        for yi in range(y-1,y-8,-1):
            if Piece.positionValide(x,yi):
                if self.plateau[yi][x] is not None:
                    if self.plateau[yi][x].couleur != self.couleur:
                        portee.append((x,yi))
                    break
                portee.append((x,yi))
        for yi in range(y+1,y+8):
            if Piece.positionValide(x,yi):
                if self.plateau[yi][x] is not None:
                    if self.plateau[yi][x].couleur != self.couleur:
                        portee.append((x,yi))
                    break
                portee.append((x,yi))
        return portee

