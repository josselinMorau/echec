from .piece import Piece
from copy import deepcopy

class Cavalier(Piece):
    """
    Classe representant un cavalier dans un jeu d'echec.
    """

    def __init__(self,couleur,posX,posY,plateau=deepcopy(Piece.PLATEAU)):
        super().__init__('cavalier',couleur,posX,posY,plateau)
    
    def portee(self,rec=True):
        portee = []
        x,y = self.posX,self.posY
        for xi in [+2,+1,-1,-2]:
            dy = 1 + (abs(xi) == 1)
            for yi in [+dy,-dy]:
                if Piece.positionValide(x+xi,y+yi):
                    if self.plateau[y+yi][x+xi] is None or self.plateau[y+yi][x+xi].couleur != self.couleur:
                        portee.append((x+xi,y+yi))
        return portee

