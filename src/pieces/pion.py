from .piece import Piece
from .reine import Reine
from .cavalier import Cavalier
from .fou import Fou
from .tour import Tour
from copy import deepcopy

class Pion(Piece):
    """
    Classe representant un pion dans un jeu d'echec.
    """

    def __init__(self,couleur,posX,posY,plateau=deepcopy(Piece.PLATEAU)):
        super().__init__('pion',couleur,posX,posY,plateau)
    
    def portee(self,rec=True):
        portee = []
        x,y= self.posX,self.posY
        (y,dy,posDep) = (y+1,1,1) if self.couleur == 'noir' else (y-1,-1,6)
        if self.plateau[y][x] is None:
            portee.append((x,y))
            if self.posY == posDep and self.plateau[y+dy][x] is None:
                portee.append((x,y+dy))
        for xi in [x-1,x+1]:
            if Piece.positionValide(xi,y) and Piece.positionEnnemie(xi,y,self.couleur,self.plateau):
                    portee.append((xi,y))
        return portee
    
    def promouvoir(self,nom):
        """
        Remplace le pion par la piece indiquee.
        """
        piece = None
        if nom == 'reine':
            piece = Reine(self.couleur,self.posX,self.posY,self.plateau)
        elif nom == 'cavalier':
            piece = Cavalier(self.couleur,self.posX,self.posY,self.plateau)
        elif nom == 'fou':
            piece = Fou(self.couleur,self.posX,self.posY,self.plateau)
        elif nom == 'tour':
            piece = Tour(self.couleur,self.posX,self.posY,self.plateau)
        if piece is not None:
            self.plateau[self.posY][self.posX] = piece
