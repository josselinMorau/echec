from .piece import Piece
from copy import deepcopy

class Roi(Piece):
    """
    Classe representant un roi dans un jeu d'echec.
    """

    def __init__(self,couleur,posX,posY,plateau=deepcopy(Piece.PLATEAU)):
        super().__init__('roi',couleur,posX,posY,plateau)
    
    def deplacer(self,pos,simu=False):
        """
        Deplace la piece a la position indiquee. Si une piece s'y trouve, la detruit.
        Retourne la piece detruite ou None si aucune piece n'est detruite.
        Si la piece est une tour alliee, effectue un roque a la place.
        """
        (posX,posY) = pos
        pieceDetruite = None
        cible = self.plateau[posY][posX]
        if cible is not None and cible.couleur == self.couleur and cible.nom == 'tour':
            self.roque(cible,simu)
        else:
            pieceDetruite = super().deplacer(pos,simu)
        return pieceDetruite
    
    def roque(self,tour,simu=False):
        """
        Effectue un roque avec la tour indiquee.
        """
        newPosRoiX = self.posX
        newPosTourX = tour.posX
        if self.posX < tour.posX:
            newPosRoiX+=2
            newPosTourX-=2
        else:
            newPosRoiX-=2
            newPosTourX+=3
        self.deplacer((newPosRoiX,self.posY),simu)
        tour.deplacer((newPosTourX,tour.posY),simu)
    
    def portee(self,rec=True):
        portee = []
        x,y = self.posX,self.posY
        for xi in range(x-1,x+2):
            for yi in range(y-1,y+2):
                if ((x,y) != (xi,yi) and 
                Piece.positionValide(xi,yi) and 
                (self.plateau[yi][xi] is None or
                self.plateau[yi][xi].couleur != self.couleur)):
                    portee.append((xi,yi))
        return portee

