from .piece import Piece
from copy import deepcopy

class Fou(Piece):
    """
    Classe representant un fou dans un jeu d'echec.
    """

    def __init__(self,couleur,posX,posY,plateau=deepcopy(Piece.PLATEAU)):
        super().__init__('fou',couleur,posX,posY,plateau)
    
    def portee(self,rec=True):
        portee = []
        x,y = self.posX,self.posY
        for dx in [-1,+1]:
            for dy in [-1,+1]:
                xi,yi = x+dx,y+dy
                while Piece.positionValide(xi,yi):
                    if self.plateau[yi][xi] is not None:
                        if self.plateau[yi][xi].couleur != self.couleur:
                            portee.append((xi,yi))
                        break
                    portee.append((xi,yi))
                    xi+=dx
                    yi+=dy
        return portee
