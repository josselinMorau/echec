"""
Pour jouer au jeu dans le terminal. Sans interface cliquable.
"""

from pynput import keyboard as kb
from time import sleep
from . import pieces as pc
from copy import deepcopy
import sys
import os
from colorama import Fore,Style
from .jeu import enEchec, creerDictPortee, initPlateau, verifPromotion
from .ordi import choisirCoup

CYAN = Fore.CYAN #curseur
YELLOW = Fore.YELLOW #cases
BLUE = Fore.BLUE #cases
GREEN = Fore.GREEN #portee
RED = Fore.RED #piece
END = Style.RESET_ALL

def quitter():
    """
    Quitte le programme.
    """
    for pt in ['','.','..','...']:
        refresh()
        print('Arret en cours'+pt)
        sleep(0.2)
    sleep(0.8)
    sys.exit()

def refresh():
    """
    Rafraichit l'ecran.
    """
    os.system('cls' if os.name=='nt' else 'clear')

def menu(val,mode=1):
    """
    Affiche le menu.
    """
    curseur = {1:"  ", 2:"  ", 3:"  "}
    curseur[val]="->"
    if mode == 1:
        #menu principal
        menu = "%s : Partie entre joueurs\n%s : Partie contre l'ordinateur\n%s : Quitter\n" % (curseur[1],curseur[2],curseur[3])
    else:
        #menu choix couleur
        menu = "%s : Blanc\n%s : Noir\n%s : Retour\n" % (curseur[1],curseur[2],curseur[3])
    print(menu)


def echiquier(plateau,joueurEnCours='',verifEchec=False,curseur=(),piecePos=(),portee=[],promotion=-1):
    """
    Affiche l'echiquier.
    """
    def stringify(case):
        if case is None:
            return ' '+END
        return str(case)
    ligne = '+'+'+---+'*8+'+\n'
    string =ligne
    couleur = YELLOW
    for y in range(8):
        string += '|'
        couleur = YELLOW if couleur == BLUE else BLUE
        for x in range(8):
            couleur = YELLOW if couleur == BLUE else BLUE
            if (x,y) == curseur :
                string+=CYAN+'{ '+stringify(plateau[y][x])+CYAN+' }'+END
            elif (x,y) == piecePos:
                string+=RED+'| '+stringify(plateau[y][x])+RED+' |'+END
            elif (x,y) in portee:
                string+=GREEN+'| '+stringify(plateau[y][x])+GREEN+' |'+END
            else:
                string+=couleur+'| '+stringify(plateau[y][x])+couleur+' |'+END
        string += '|\n'+ligne
    print(string)
    if joueurEnCours:
        print('Tour des %ss'%joueurEnCours)
        if verifEchec and enEchec(plateau,joueurEnCours):
            print('Roi %s en echec.' % joueurEnCours)
    if promotion >= 0:
        promos = ['reine','cavalier','tour','fou']
        print('Choisissez la promotion:\n%s[%s]%s' % ('<-' if promotion > 0 else '  ',promos[promotion],'->' if promotion < 3 else ''))

def chercherDestination(direction,position,portee):
    """
    Cherche et retourne la prochaine position a portee dans la direction souhaitee.
    """
    (x,y) = position
    xi,yi = x,y
    if direction == 'up' or direction == 'down':
        while True:
            y = y-1 if direction == 'up' else y+1
            if y < 0 or y > 7 :
                break
            if (x,y) in portee:
                return (x,y)
            for (dep,fin,pas) in [(x-1,-1,-1),(x+1,8,1)]:
                for i in range(dep,fin,pas):
                    if (i,y) in portee:
                        return (i,y)
    else:
        (dep,fin,pas) = (x-1,-1,-1) if direction == 'left' else (x+1,8,1)
        for i in range(dep,fin,pas):
            if (i,y) in portee:
                return (i,y)
    return (xi,yi)

def jouer(ordi='aucun'):
    """
    Lance une partie d'echecs.
    """
    plateau = deepcopy(pc.Piece.PLATEAU)
    pions = {'blanc':[],'noir':[],'roi-blanc':None,'roi-noir':None}
    initPlateau(plateau,pions)
    joueurEnCours='noir'
    ragequit = False
    mat = False
    pat = False 
    def jouerKey(key):
        if key == kb.Key.up:
            jouerKey.val = 'up'
        elif key == kb.Key.down:
            jouerKey.val = 'down'
        elif key == kb.Key.left:
            jouerKey.val = 'left'
        elif key == kb.Key.right:
            jouerKey.val = 'right'
        elif key == kb.KeyCode.from_char('q'):
            jouerKey.val = 'q'
        elif key == kb.KeyCode.from_char('b'):
            jouerKey.val = 'b'
        elif key == kb.Key.space:
            jouerKey.val = 'space'
        return False
    jouerKey.val = 'none'
    while True:
        if ragequit:
            break
        joueurEnCours = 'blanc' if joueurEnCours == 'noir' else 'noir'
        dictPortee = creerDictPortee(plateau,joueurEnCours)
        if not dictPortee:
            if enEchec(plateau,joueurEnCours):
                mat = True
            else:
                pat = True
            break
        if joueurEnCours == ordi:
            refresh()
            echiquier(plateau,joueurEnCours,True)
            sleep(0.8)
            dep,dest = choisirCoup(plateau,dictPortee,joueurEnCours)
            pionCourant = plateau[dep[1]][dep[0]]
            refresh()
            echiquier(plateau,joueurEnCours,True,dep)
            sleep(0.8)
            refresh()
            echiquier(plateau,joueurEnCours,True,dest,dep,dictPortee[dep])
            sleep(0.8)
            pionCourant.deplacer(dest)
            refresh()
            echiquier(plateau,joueurEnCours,True)
            sleep(0.8)
        else:
            curseur = list(dictPortee.keys())[0]
            pionCourant = plateau[curseur[1]][curseur[0]]
            refresh()
            echiquier(plateau,joueurEnCours,True,curseur)
            sleep(1)
            # phases: 0 : 'selectPiece' , 1 : 'selectDestination' , 2 :'finTour'
            phaseDeJeu = 0
            while phaseDeJeu != 2:
                jouerKey.val = 'none'
                with kb.Listener(on_release = jouerKey) as listener:
                    listener.join()
                for direction in ['up','down','left','right']:
                    if jouerKey.val == 'q':
                            phaseDeJeu = 2
                            ragequit = True
                            break
                    if phaseDeJeu == 0:
                        if jouerKey.val == direction:
                            curseur = chercherDestination(direction,curseur,dictPortee.keys())
                            pionCourant = plateau[curseur[1]][curseur[0]]
                            refresh()
                            echiquier(plateau,joueurEnCours,True,curseur)
                            sleep(0.1)
                            break
                        if jouerKey.val == 'space':
                            porteeCourante = dictPortee[curseur]
                            destinationPos = porteeCourante[0]
                            phaseDeJeu = 1
                            refresh()
                            echiquier(plateau,joueurEnCours,True,destinationPos,curseur,porteeCourante)
                            sleep(0.1)
                            break
                    if phaseDeJeu == 1:
                        if jouerKey.val == direction:
                            destinationPos = chercherDestination(direction,destinationPos,porteeCourante)
                            refresh()
                            echiquier(plateau,joueurEnCours,True,destinationPos,curseur,porteeCourante)
                            sleep(0.1)
                            break
                        if jouerKey.val == 'space':
                            pieceDetruite = pionCourant.deplacer(destinationPos)
                            if pieceDetruite is not None:
                                pions['noir'].remove(pieceDetruite) if joueurEnCours == 'blanc' else pions['blanc'].remove(pieceDetruite)
                            phaseDeJeu = 2
                            if pionCourant.nom == "pion":
                                pionPromu = verifPromotion(plateau,joueurEnCours)
                                if pionPromu is not None:
                                    promotion = 0
                                    refresh()
                                    echiquier(plateau,joueurEnCours=joueurEnCours,promotion=promotion)
                                    sleep(1)
                                    while True:
                                        with kb.Listener(on_release = jouerKey) as listener:
                                            listener.join()
                                        if jouerKey.val == 'left':
                                            promotion = max(0,promotion-1)
                                            refresh()
                                            echiquier(plateau,joueurEnCours=joueurEnCours,promotion=promotion)
                                            sleep(0.1)
                                        if jouerKey.val == 'right':
                                            promotion = min(3,promotion+1)
                                            refresh()  
                                            echiquier(plateau,joueurEnCours=joueurEnCours,promotion=promotion)
                                            sleep(0.1)
                                        if jouerKey.val == 'space':
                                            break
                                    promos = ['reine','cavalier','tour','fou']
                                    pionPromu.promouvoir(promos[promotion])
                            break
                        if jouerKey.val == 'b':
                            phaseDeJeu = 0
                            refresh()
                            echiquier(plateau,joueurEnCours,True,curseur)
                            sleep(0.1)
                            break
    refresh()
    echiquier(plateau)
    if ragequit or mat:
        if mat:
            print('Echec et Mat.')
        else:
            print('Abandon.')
        joueurEnCours = 'blanc' if joueurEnCours == 'noir' else 'noir'
    if pat:
        print('Pat. Partie Nulle')
    else:
        print('Victoire des %ss.'%joueurEnCours)
    print('Appuyer sur q pour quitter.')
    jouerKey.val = 'none'
    while jouerKey.val != 'q':
        with kb.Listener(on_release = jouerKey) as listener:
            listener.join()
        pass

def demarrer():
    """
    Demarre le programme en mode terminal.
    """
    select = 1
    refresh()
    menu(select)
    def mainKey(key):
        if key == kb.Key.up:
            mainKey.val = 'up'
        elif key == kb.Key.down:
            mainKey.val = 'down'
        elif key == kb.KeyCode.from_char('q'):
            mainKey.val = 'q'
        elif key == kb.Key.space:
            mainKey.val = 'space'
        return False
    modeMenu = 1
    while True:
        mainKey.val = 'none'
        with kb.Listener(on_release = mainKey) as listener:
            listener.join()
        if mainKey.val == 'up':
            select = max(1,select-1)
            refresh()
            menu(select,modeMenu)
            sleep(0.1)
        elif mainKey.val == 'down':
            select = min(3,select+1)
            refresh()
            menu(select,modeMenu)
            sleep(0.1)
        elif mainKey.val == 'q':
            quitter()
        elif mainKey.val == 'space' and modeMenu == 1:
            if select == 3:
                quitter()
            elif select == 2:
                modeMenu = 2
                select = 1
                refresh()
                menu(select,modeMenu)
                sleep(0.1)
            else:
                jouer()
                refresh()
                menu(select)
                sleep(0.1)
        elif mainKey.val == 'space' and modeMenu == 2:
            if select == 3:
                modeMenu = 1
                select = 1
                refresh()
                menu(select,modeMenu)
                sleep(0.1)
            else:
                ordi = 'noir' if select == 1 else 'blanc'
                jouer(ordi)
                modeMenu = 1
                refresh()
                menu(select,modeMenu)
                sleep(0.1)
