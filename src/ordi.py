"""
Gere le comportement de l'adversaire controle par l'ordinateur.
"""
from .jeu import creerDictPortee
import random as rdm
from copy import deepcopy

def nbCasesRoi(plateau,posRoi,portees):
    """
    Retourne le nombre de cases adjacentes au roi libres ou occupees par des piece alliees
    se trouvant dans la portee donnee.
    """
    (posX,posY) = posRoi
    couleur = plateau[posY][posX].couleur
    nbCases = 0
    for portee in portees:
        for posI in range(posX-1,posX+2):
            for posJ in range(posY-1,posY+2):
                if (posI,posJ) != posRoi and (posI,posJ) in portee and (plateau[posJ][posI] is None or plateau[posJ][posI].couleur != couleur):
                    nbCases += 1
    return nbCases

def calculerScore(plateau,couleur,piecePrise='aucune'):
    """
    Calcule et retourne le score d'un etat du plateau d'echecs.
    Le score varie en fonction des pieces capturees des 2 camps.
    """
    valeurs = {'pion':1.0, 'fou': 3.33, 'cavalier': 3.2, 'tour': 5.1, 'reine': 8.8, 'roi': 10.0}
    porteeOrdi = creerDictPortee(plateau,couleur)
    couleurJoueur = 'blanc' if couleur == 'noir' else 'noir'
    porteeJoueur = creerDictPortee(plateau,couleurJoueur)
    posRoiJoueur, posRoiOrdi = (),()
    for ligne in plateau:
        for case in ligne:
            if case is not None and case.nom == 'roi':
                if case.couleur == couleurJoueur:
                    posRoiJoueur = case.pos
                else:
                    posRoiOrdi = case.pos
    score = 0.0
    if piecePrise != 'aucune':
        score += valeurs[piecePrise]
    for val in [0.5,-1.0]:
        porteeCle = porteeOrdi if val == -1.0 else porteeJoueur
        porteeVal = porteeOrdi if val == 0.5 else porteeJoueur
        posRoi = posRoiJoueur if val == 0.5 else posRoiOrdi
        for (posX,posY) in porteeCle.keys():
            for portee in porteeVal.values():
                if (posX,posY) in portee:
                    score += valeurs[plateau[posY][posX].nom]*val
        score += nbCasesRoi(plateau,posRoi,porteeVal)*val      
    return score

def choisirCoup(plateau,portee,couleur):
    """
    Evalue tous les coups possibles par l'ordinateur et retourne celui avec le plus haut score.
    Si plusieurs coups sont equivalents, retourne un au hasard.
    """
    nbPieces = -1
    piecesIndex = {'pion':0, 'fou': 1, 'cavalier': 2, 'tour': 3, 'reine': 4, 'roi': 5}
    multiplicateurs = [
        #pon, fou, cav, tor, rne, roi
        [0.8, 1.2, 0.8, 1.2, 0.9, 1.0], # nbPieces<=8
        [0.8, 1.2, 0.8, 1.1, 1.2, 1.0], # nbPieces<=16
        [1.0, 1.1, 1.0, 1.1, 1.0, 1.0], # nbPieces<=24
        [1.2, 1.0, 1.2, 1.0, 0.9, 1.0]  # nbPieces<=32
    ]
    for ligne in plateau:
        for case in ligne:
            if case is not None:
                nbPieces += 1
    nbPieces //= 8
    listeCoups = []
    scoreMax = -float('inf')
    for cle in portee.keys():
        for dest in portee[cle]:
            plateauSimu = deepcopy(plateau)
            dep = plateauSimu[cle[1]][cle[0]]
            pcId = piecesIndex[dep.nom] if dep else 5 
            multi = multiplicateurs[nbPieces][pcId]
            cible = plateauSimu[dest[1]][dest[0]]
            piecePrise = 'aucune' if cible is None else cible.nom
            plateauSimu[cle[1]][cle[0]].deplacer(dest,simu=True)
            score = multi*calculerScore(plateauSimu,couleur,piecePrise)
            if score == scoreMax:
                listeCoups.append([cle,dest])
            elif score > scoreMax:
                scoreMax = score
                listeCoups = [[cle,dest]]
    coup = rdm.choice(listeCoups)
    return coup[0],coup[1]
