"""
Demarre le programme.
"""
import sys

if __name__ == '__main__':
    try:
        if len(sys.argv) > 1 and sys.argv[1] == "-i":
            from src.partieInterface import demarrer
        else :
            from src.partie import demarrer
        demarrer()
    except ImportError:
        print('ImportError: Make sure you got all the required libraries.')
        raise
    except SystemExit:
        pass
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise