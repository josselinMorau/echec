# Introduction

Voici un programme simple en python pour lancer une partie d'échecs directement jouable sur un terminal uniquement avec les touches de votre clavier ou bien sur une interface cliquable.

# Prérequis

## Version de Python

* Python 3.x

## Librairies

Pour le mode terminal:

* pynput
* colorama

Pour le mode interface:

* pygame

Pour installer la librairie manquante, utilisez la commande pip install nom-librairie.
Pour plus d'informations, se référer à la [documentation officielle](https://docs.python.org/fr/3.6/installing/index.html).

## Police

En mode terminal, si vous utilisez l'invite de commande Windows, vous devez utiliser une police compatible avec les caractères utilisés par le programme. Pour changer la police d'affichage de l'invite de commande, faites un clic droit sur l'icône en haut
à gauche de la fenêtre, puis cliquez sur 'Propriétés' et sélectionnez l'onglet 'Police'.
Choisissez la police 'DejaVu Sans Mono' ou tout autre police supportant [les caractères d'un jeu d'échecs](https://fr.wikipedia.org/wiki/Symboles_d%27%C3%A9checs_en_Unicode).

# Comment jouer?

## Lancer le programme

* Ouvrez le terminal de Linux ou l'invite de commande Windows à l'emplacement où se situe le fichier main.py.
* Utilisez la commande cd chemin-vers-le-dossier/echec si vous n'êtes pas déjà à l'emplacement indiqué.
* Utilisez la commande `python main.py` pour éxecuter le programme en mode terminal.
* Utilisez la commande `python main.py -i` pour éxecuter le programme en mode interface.

## Jouer

### Mode terminal

Pour naviguer dans le menu, utilisez les flèches directionnelles de votre clavier.
Appuyez sur espace pour confirmer votre sélection.
Appuyez sur la touche 'q' ou sélectionnez 'Quitter' pour arrêter le programme.

Une fois en jeu:

Les blancs commencent.<br/>
Déplacez le curseur bleu avec les flèches directionnelles pour choisir la pièce que vous voulez jouer.<br/>
Le curseur saute directement sur les pièces que vous avez le droit de jouer ce tour-ci conformément aux règles du jeu.<br/>
Confirmez la pièce avec la touche 'espace'.<br/>
Sélectionnez ensuite la case où déplacer votre pièce.<br/>
Si vous souhaîtez revenir en arrière pour sélectionner une autre pièce, appuyer sur la touche 'b'.<br/>
Confirmez le déplacement avec la touche 'espace'.<br/>
C'est ensuite au tour de l'adversaire.<br/>
Pendant votre tour, vous pouvez abandonner à tout moment en appuyant sur la touche 'q'.<br/>

Les tours s'enchaînent jusqu'à qu'un joueur soit échec et mat, pat ou ait abandonné.

Appuyez ensuite sur 'espace' pour revenir au menu principal.

### Mode interface:

Déplacez le curseur de votre souris pour naviguer dans le menu et cliquez pour sélectionner les options de jeu.
Fermez la fenêtre ou cliquez sur le bouton 'Quitter' pour arrêter le programme.

Une fois en jeu:

Les blancs commencent.<br/>
Déplacez le curseur puis cliquez sur la case d'une pièce jouable.<br/>
Sélectionnez ensuite la case où déplacer votre pièce en cliquant dessus.<br/>
Cliquer ailleurs annulera votre sélection.<br/>
C'est ensuite au tour de l'adversaire.<br/>
Pendant votre tour, vous pouvez abandonner à tout moment en cliquant sur le bouton 'ABANDONNER'.<br/>
Vous pouvez mettre en évidence les pièces jouables en cliquant sur le bouton 'PIÈCES JOUABLES'.<br/>

Les tours s'enchaînent jusqu'à qu'un joueur soit échec et mat, pat ou ait abandonné.

Cliquez ensuite pour revenir au menu principal.